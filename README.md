# Experimental Assessment of External Validity Claims in Jupyter Notebooks

This repository contains the data and code for the paper "Experimental Assessment of External Validity Claims in 
Jupyter Notebooks" by [Aurelia Maghasi](https://gitlab.com/salliamu), for a bachelor's thesis at the 
[University of Munich](https://www.lmu.de/en/).

## Abstract
The paper presents an experimental study on the external validity claims about Jupyter Notebooks.  
 It seeks to reproduce the results of the following papers/artifacts:
1. [Restoring the executability of jupyter notebooks by automatic upgrade of deprecated apis.](https://dl-acm-org.emedien.ub.uni-muenchen.de/doi/10.1109/ASE51524.2021.9678889) - Relancer 
2. [Restoring execution environments of jupyter notebooks.](https://arxiv.org/abs/2103.02959) - SnifferDog
3. [Assessing and restoring reproducibility of jupyter notebooks.](https://dl.acm.org/doi/10.1145/3324884.3416585) - Osiris

For positively reproduced results, the artifacts are ran again on a different dataset.  
In this project, data processing is done on the datasets of Relancer & SnifferDog.  
Then, baseline results are obtained by running the SnifferDog and Osiris on the processed datasets.  
Finally, SnifferDog and Osiris are run once again, on different datasets, to obtain validation results.

## Prerequisites

- Linux OS
- Free space on the system drive of at least 25 GB free space.
- Docker (tested with version docker version; and podman version 3.4.4) [Official Docker Installation Guide](https://docs.docker.com/get-docker/)

The tool was tested on the following machines:
- an AMD EPYC 7713 64-Core Processor, 2.0TiB RAM, running Ubuntu 22.04.
- an Intel(R) Core(TM) i9-8950HK CPU @ 2.90GHz, 32 GiB RAM, running Windows 10.

## Getting started

1. Clone this repository in a folder named external_validity
   ```bash
   git clone https://gitlab.com/thesis-bachelor/experiments.git external_validity
   ```

2. Create a `my_secrets.py` file and add the following variable:  
   `GITHUB_TOKEN`: Create a [personal access token](https://docs.github.com/en/github/authenticating-to-github/creating-a-personal-access-token) with the `repo` scope. 

3. Create an API token by following the steps in the menu point 'authentication' in the 
[Kaggle API Documentation](https://www.kaggle.com/docs/api).
Copy the `kaggle.json` file to the `docker/experiments` folder.


## Container Setup & Execution
Each step and artifact is run in a separate Docker container.
To build the containers, and complete the setup, follow the instructions below:

### Data Processing  
1. Build the container and navigate back to the project root.
   ```bash
   cd docker/experiments
   docker build -f Dockerfile -t experiments .
   cd ../..
   ```

2. Run the container from the project root, mounting the repository as a volume.
   ```bash
   docker run --name experiments -v </path/to/project/folder>:/home/external_validity -it experiments
   ```

3. Activate the conda environment.
   ```bash
   conda activate experiments
   ```
   
4. Run the data processing script.
   ```bash
   python main.py preprocess
   ```
   
Time estimation: ~30 mins on an Intel(R) Core(TM) i9-8950HK CPU @ 2.90GHz, 32 GiB RAM, running Windows 10.
During the run, logs/preprocessing.file is generated and updated with the progress of the script. To get an idea of how many
repositories have been processed so far, run the following command:
   ```bash
   grep -c "results: {'id_name'" /home/external_validity/logs/preprocessing.log
   ```

   
### Establishing baselines and Validation

#### Osiris
**NOTE:** Before running the Osiris artifact, the data processing step must be completed first.

1. Build the container.
   ```bash
   cd docker/osiris
   docker build -f Dockerfile -t osiris .
   cd ../..
   ```
   
2. Run the container from the project root, mounting the repository as a volume.
   ```bash
    docker run --name osiris -v </path/to/project/folder>:/home/external_validity -it osiris
    ```
   
3. Check the list of environments that were successfully created. There should be 3 environments: 
    `Osiris_default`, `Osiris_py35`, `Osiris_py36`, `Osiris_py37`.
      ```bash
      conda env list
      ```
   
4. Activate the default environment.
    ```bash
    conda activate Osiris_default
    ```
   
5. To reproduce the baseline results from the paper, run the following command. 
   ```bash
   python main.py baseline --artifact osiris
   ```

6. To reproduce the validation results from the paper, run the following command. 
   ```bash
   python main.py validate --artifact osiris
   ```

#### SnifferDog
**NOTE:** Before running the SnifferDog artifact, the data processing step must be completed first.

1. Build the container.
   ```bash
   cd docker/snifferdog
   docker build -f Dockerfile -t snifferdog .
   cd ../..
   ```
   
2. Run the container from the project root, mounting the repository as a volume.
   ```bash
    docker run --name snifferdog -v </path/to/project/folder>:/home/external_validity -it snifferdog
    ```
   
3. Activate the conda environment.
    ```bash
    conda activate snifferdog
    ```
   
4. Pip install the requirements.
   ```bash
   pip install -r docker/snifferdog/requirements.txt
   ```
   
5. To reproduce the baseline results from the paper, run the following command. 
   ```bash
    python main.py baseline --artifact snifferdog
    ```
   
6. To reproduce the validation results from the paper, run the following command. 
   ```bash
    python main.py validate --artifact snifferdog
    ```


#### Results
The results of the experiments are stored in the `output` folder.
These can be analyzed by running the following commands while in the `experiments` container:
   ```bash
    conda activate experiments
    python main.py report --artifact raw_data    # to get the venn diagram on the raw data
    python main.py report --artifact osiris      # show results for the osiris artifact
    python main.py report --artifact snifferdog  # show results for the snifferdog artifact
    python main.py report --artifact all         # show results for all results
   ```
These results correspond to the tables and figures presented in the thesis paper: `Experimental Assessment of External Validity Claims in Jupyter Notebooks`.


## Structure
For more details on how to use this tool, see the [Structure](docs/structure.md).

