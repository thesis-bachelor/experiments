from multiprocessing import Pool

import numpy as np
import pandas as pd


def helper_function(args):
    chunk, method, kwargs = args
    return method(chunk, **kwargs)


def do_threading(input_file, output_file, method, num_chunks=100, **kwargs):
    df = pd.read_csv(input_file)

    # Divide the input data into chunks
    chunk_size = int(np.ceil(len(df) / num_chunks))

    chunks = [df[i:i + chunk_size] for i in range(0, df.shape[0], chunk_size)]
    print(f"Chunk size: {chunk_size}")
    print(f"Number of chunks: {len(chunks)}")

    # Process the chunks in parallel
    with Pool(processes=num_chunks) as pool:
        print("Starting pool...")
        arguments = [(chunk, method, kwargs) for chunk in chunks]
        results = pool.map(helper_function, arguments)

    # Combine the results into a single dataframe
    results_df = pd.concat([pd.DataFrame(res) for res in results])
    print(f"Results dataframe: {results_df}")

    # Save the results to a CSV file
    results_df.to_csv(output_file, index=False)
