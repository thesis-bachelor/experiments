import ast
import json
import math
import os
import subprocess
import time

import yaml

from constants import PROJECT_DIR, CONDA_SH, OSIRIS_BINARY, OSIRIS_BASELINE_RESULTS, \
    OSIRIS_VALIDATION_RESULTS_ON_SNIFFERDOG, OSIRIS_VALIDATION_RESULTS_ON_RELANCER, \
    SNIFFERDOG_BASELINE_RESULTS, SNIFFERDOG_VALIDATION_RESULTS_ON_RELANCER, SNIFFERDOG_YML_DIR, SNIFFERDOG_DIR, \
    ENV_PREFIX, OSIRIS_SAMPLE, SNIFFERDOG_DATASET, RELANCER_DATASET, OSIRIS_DIR, OSIRIS_RAW_DATASET
from logs.helper import ExperimentLogger
from threader import do_threading


class Artifacts:
    def __init__(self):
        pass

    @staticmethod
    def isNotebookPathsEmpty(notebooks):
        return not notebooks or (isinstance(notebooks, float) and math.isnan(notebooks))


class OsirisArtifact(Artifacts):
    @staticmethod
    def run_strategy(command, logger):
        logger.info(f"Running command: {command}")
        start_time = time.time()
        end_time = None

        try:
            process = subprocess.run(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, check=True)
            end_time = time.time()
            logger.info(f"Command finished with following output: {process.stdout.decode('utf-8')}")
            logger.info(f"Command finished with following error: {process.stderr.decode('utf-8')}")
            return process.stdout.decode('utf-8').strip(), process.stderr.decode('utf-8').strip(), end_time - start_time, None
        except subprocess.CalledProcessError as e:
            logger.error(f"Error running command: {e}")
            return None, None, end_time - start_time if end_time else None, e

    def establish_baseline(self):
        do_threading(OSIRIS_RAW_DATASET, OSIRIS_BASELINE_RESULTS,
                     self.run_osiris_executable, logger_name="osiris_baseline")

    def run_validation(self):
        # On SnifferDog subjects
        do_threading(SNIFFERDOG_DATASET, OSIRIS_VALIDATION_RESULTS_ON_SNIFFERDOG,
                     self.run_osiris_executable(logger_name="osiris_validation_snifferdog"))

        # On Relancer subjects
        do_threading(RELANCER_DATASET, OSIRIS_VALIDATION_RESULTS_ON_RELANCER,
                     self.run_osiris_executable(logger_name="osiris_validation_relancer"))

    def run_osiris_executable(self, df, logger_name):
        logger = ExperimentLogger.init_logger()
        results = []
        os.chdir(OSIRIS_DIR)
        for _, row in df.iterrows():
            id_name = row['id_name']
            notebooks = row['notebook_paths']

            base_info = {
                'id_name': id_name,
            }

            if self.isNotebookPathsEmpty(notebooks):
                base_info['errors'] = "Notebook paths are empty"
                results.append(base_info)
                continue

            for notebook in ast.literal_eval(notebooks):
                notebook_path = os.path.join(PROJECT_DIR, notebook).replace(" ", "\\ ")
                base_info['notebook_path'] = notebook_path

                # osiris requires .ipynb extension for notebook path
                if os.path.exists(notebook_path.replace(".py", ".ipynb")):
                    notebook_path = notebook_path.replace(".py", ".ipynb")
                else:
                    base_info['errors'] = "Notebook path does not exist as ipynb"
                    results.append(base_info)
                    continue

                logger.info(f"Running osiris on {notebook_path}")

                base_command = f"bash -c 'source {CONDA_SH} && source {OSIRIS_BINARY} {notebook_path}"

                # Normal Execution Strategy
                command_normal = f"{base_command} normal'"
                stdout_normal, stderr_normal, time_normal, error = self.run_strategy(command_normal, logger)

                if error or stderr_normal:
                    base_info['errors'] = error
                    results.append(base_info)
                    continue

                # Extracts executability and python version from stdout
                executability = stdout_normal.split(":")[1].split("\n")[0].strip() \
                    if len(stdout_normal.split(":")) > 0 else "Couldn't find executability"

                python_version = stdout_normal.split("\n")[0] if len(stdout_normal.split("\n")) > 0 \
                    else "Couldn't find python version"

                if not executability == "True":
                    base_info['executability'] = executability
                    base_info['python_version'] = python_version
                    base_info['normal_execution_output'] = stdout_normal
                    base_info['normal_execution_error'] = stderr_normal
                    base_info['normal_execution_time'] = time_normal
                    base_info['errors'] = str(error)
                    results.append(base_info)
                    continue

                # Weak Matching Strategy
                command_weak = f"{base_command} OEC \"-m weak\"'"
                stdout_weak, stderr_weak, time_weak, error = self.run_strategy(command_weak, logger)
                if error or stderr_weak:
                    base_info['errors'] = error
                    results.append(base_info)
                    continue
                base_info['weak_execution_output'] = stdout_weak
                base_info['weak_execution_error'] = stderr_weak
                base_info['weak_execution_time'] = time_weak
                base_info['weak_matched_ratio'] = stdout_weak.split("matched ratio: ")[1].split(";")[0].strip() \
                    if executability == "True" and "matched ratio: " in stdout_weak else None
                base_info['weak_matched_cells_count'] = stdout_weak.split("number of matched cells: ")[1].split(";")[
                    0].strip() \
                    if executability == "True" and "number of matched cells: " in stdout_weak else None
                base_info['errors'] = str(error)

                # Strong Matching Strategy
                command_strong = f"{base_command} OEC \"-m strong\"'"
                stdout_strong, stderr_strong, time_strong, error = self.run_strategy(command_strong, logger)
                if error or stderr_strong:
                    base_info['errors'] = error
                    results.append(base_info)
                    continue
                base_info['strong_execution_output'] = stdout_strong
                base_info['strong_execution_error'] = stderr_strong
                base_info['strong_execution_time'] = time_strong
                base_info['strong_matched_ratio'] = stdout_strong.split("matched ratio: ")[1].split(";")[0].strip() \
                    if executability == "True" and "matched ratio: " in stdout_strong else None
                base_info['strong_matched_cells_count'] = \
                stdout_strong.split("number of matched cells: ")[1].split(";")[0].strip() \
                    if executability == "True" and "number of matched cells: " in stdout_strong else None
                base_info['errors'] = str(error)

                # Best Effort Matching Strategy
                command_best_effort = f"{base_command} OEC \"-m best_effort\"'"
                stdout_best_effort, stderr_best_effort, time_best_effort, error = self.run_strategy(command_best_effort,
                                                                                                    logger)
                if error or stderr_best_effort:
                    base_info['errors'] = error
                    results.append(base_info)
                    continue
                base_info['best_effort_execution_output'] = stdout_best_effort
                base_info['best_effort_execution_error'] = stderr_best_effort
                base_info['best_effort_execution_time'] = time_best_effort
                base_info['best_effort_matched_ratio'] = stdout_best_effort.split("matched ratio: ")[1].split(";")[
                    0].strip() \
                    if executability == "True" and "matched ratio: " in stdout_best_effort else None
                base_info['best_effort_matched_cells_count'] = \
                stdout_best_effort.split("number of matched cells: ")[1].split(";")[0].strip() \
                    if executability == "True" and "number of matched cells: " in stdout_best_effort else None
                base_info['errors'] = str(error)

                results.append(base_info)

        os.chdir(PROJECT_DIR)
        return results


class SnifferDogArtifact(Artifacts):
    @staticmethod
    def generate_dependencies(notebook_path, logger):
        command = f"python lib_ver_producer.py {notebook_path}"
        logger.info(f"Generating dependencies for {notebook_path}")
        start = time.time()
        end = None

        try:
            process = subprocess.run(command, shell=True, check=True, stdout=subprocess.PIPE)
            end = time.time()
            dependencies = process.stdout.decode('utf-8').splitlines()
            logger.info(f"Dependencies for {notebook_path} are {dependencies}")
            return dependencies, end - start, None
        except subprocess.CalledProcessError as e:
            return None, end - start if end else None, e

    @staticmethod
    def create_environment_file(env_file_path, env_name, generated_requirements, python_version, logger):
        dependencies = []
        rest = []
        created = False
        error = None

        for item in generated_requirements:
            # If the item is a dictionary, extract the 'module' key
            if item.startswith('{'):
                item_dict = json.loads(item.replace("'", "\""))
                dependencies = item_dict['module']
            else:
                rest.append(item)

        for item in dependencies:
            # check if there is a substring with item in the rest list
            for s in rest:
                if item in s:
                    dependencies = list(map(lambda x: x.replace(item, s), dependencies))
        logger.info(f"Dependencies: {dependencies}")

        logger.info(f"Writing dependencies to {env_file_path}")
        prompt_toolkit = "prompt-toolkit==2.0.1" if float(python_version.split(".")[1]) <= 6 else "prompt-toolkit"
        env_yaml = {
            'name': env_name,
            'channels': ["defaults", "conda-forge"],
            'dependencies': [
                f"python={python_version}",
                "pip",
                {"pip": ["jupyter", prompt_toolkit, *dependencies]}
            ]
        }

        try:
            with open(env_file_path, 'w') as file:
                yaml.dump(env_yaml, file)
                file.close()
            created = True
        except Exception as e:
            logger.error(f"Error while writing environment file: {e}")
            error = e

        return created, dependencies, error

    @staticmethod
    def create_environment(env_file_path, logger):
        logger.info(f"Creating environment from {env_file_path}")
        command = f"conda env create -f {env_file_path}"
        timeout = 60 * 5

        try:
            process = subprocess.run(command, shell=True, check=True, stdout=subprocess.PIPE, timeout=timeout)
            return process.returncode == 0, None
        except subprocess.CalledProcessError as e:
            return False, e

    @staticmethod
    def execute_notebook(notebook_path, env_name, logger):
        logger.info(f"Executing notebook {notebook_path} in environment {env_name}")
        command = f"conda run -n {env_name} python {SNIFFERDOG_DIR}/run_jupyter.py {notebook_path}"
        timeout = 60 * 5

        try:
            process = subprocess.run(command, shell=True, check=True, stdout=subprocess.PIPE, timeout=timeout)
            return process.stdout.decode('utf-8').strip() == "success", None
        except subprocess.CalledProcessError as e:
            return False, e

    @staticmethod
    def remove_environment(env_name, logger):
        logger.info(f"Removing environment {env_name}")
        command = f"conda env remove -n {env_name} --yes"

        try:
            process = subprocess.run(command, shell=True, check=True, stdout=subprocess.PIPE)
            logger.info(f"Environment {env_name} removed: {process.stdout.decode('utf-8').strip()}")
        except subprocess.CalledProcessError as e:
            logger.error(f"Error while removing environment {env_name}: {e}")

    def establish_baseline(self):
        do_threading(SNIFFERDOG_DATASET, SNIFFERDOG_BASELINE_RESULTS,
                     self.run_snifferdog_executable, logger_name="snifferdog_baseline")

    def run_validation(self):
        # On Relancer Subjects
        do_threading(RELANCER_DATASET, SNIFFERDOG_VALIDATION_RESULTS_ON_RELANCER,
                     self.run_snifferdog_executable(logger_name="snifferdog_validation"))

    def run_snifferdog_executable(self, df, logger_name):
        logger = ExperimentLogger.init_logger(logger_name)
        results = []

        # Change directory to snifferdog
        os.chdir(SNIFFERDOG_DIR)

        for _, row in df.iterrows():
            id_name = row['id_name']
            python_version = row["python_version"]
            notebooks = row['notebook_paths']
            dataset_origin = row['dataset_origin']
            snifferdog_yml_file = f"{SNIFFERDOG_YML_DIR}/{id_name}.yml"

            base_info = {
                'id_name': id_name,
                'Env file': snifferdog_yml_file,
            }
            logger.info(f"Running SnifferDog on Repository {id_name}")

            if self.isNotebookPathsEmpty(notebooks):
                base_info['Error'] = "No notebooks"
                results.append(base_info)
                logger.info(f"Skipping repository {id_name} because it has no notebooks")
                continue

            for notebook in ast.literal_eval(notebooks):
                # SnifferDog requires .ipynb files as input: Need to set this explicitly for relancer subjects
                if "relancer" in dataset_origin:
                    notebook = notebook.replace(".py", ".ipynb")

                notebook_path = f"{PROJECT_DIR}/{notebook}".replace(" ", "\\ ")

                # Skip if notebook doesn't exist
                if not os.path.exists(notebook_path):
                    base_info['Error'] = f"Notebook {notebook_path} doesn't exist"
                    results.append(base_info)
                    logger.info(f"Skipping notebook {notebook_path} because it doesn't exist")
                    continue

                # Generate Requirements using SnifferDog
                generated_requirements, exec_time, error = self.generate_dependencies(notebook_path, logger)
                base_info['Execution Time'] = exec_time
                if error:
                    base_info['Error'] = f"Error generating requirements for {notebook_path}"
                    results.append(base_info)
                    logger.info(f"Skipping notebook {notebook_path} because of error generating requirements")
                    continue

                # Create environment file from generated requirements
                env_file_path = notebook_path.replace(".ipynb", ".yml")
                file = notebook_path.split("/")[-1].replace(".ipynb", "")
                env_name = f"{ENV_PREFIX}{id_name}_{file}"

                file_created, dependencies, error = self.create_environment_file(env_file_path, env_name,
                                                                                 generated_requirements, python_version,
                                                                                 logger)
                if not file_created:
                    base_info['Error'] = f"Error creating environment file: {error}"
                    results.append(base_info)
                    logger.info(f"Skipping notebook {notebook_path} because of error creating environment file")
                    continue

                base_info['Dependencies'] = dependencies

                # Create environment from environment file
                env_created, error = self.create_environment(env_file_path, logger)
                if not env_created:
                    base_info['Error'] = f"Own yml file fails: {error}"

                    logger.info("Trying to create environment with the already provided yml file")
                    created, err = self.create_environment(snifferdog_yml_file, logger)
                    if not created:
                        base_info['Error'] = f"Both environments fail: Provided yml file:{err} AND own yml file:{error}"
                        results.append(base_info)
                        logger.info(f"Skipping notebook {notebook_path} because of error creating environment")
                        continue

                # Execute the notebook in the environment
                # Select the correct environment name.Either own generated environment or the one provided which
                # is given in the format:  f"our{id_name}"
                env = env_name if env_created else f"our_{id_name}"
                executable, error = self.execute_notebook(notebook_path, env, logger)
                base_info["IsExecutable"] = executable
                base_info['Error'] = error if error else "No error"

                results.append(base_info)
                logger.info(f"Finished running SnifferDog on notebook {notebook_path}")

                # Remove environment
                self.remove_environment(env, logger)

        # Change directory back to project root
        os.chdir(PROJECT_DIR)
        return results

