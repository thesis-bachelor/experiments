#!/bin/bash

conda install conda=4.6.11
conda create -n Osiris_default anaconda python=3.7 gitpython
conda create -n Osiris_py35 anaconda python=3.5
conda create -n Osiris_py36 anaconda python=3.6
conda create -n Osiris_py37 anaconda python=3.7