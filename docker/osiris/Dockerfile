# Dockerfile
FROM continuumio/anaconda3

LABEL maintainer="Aurelia Maghasi a.maghasi@campus.lmu.de"

ENV LANG C.UTF-8

# Install sofware properties common
RUN \
  apt-get update && \
  apt-get install -y software-properties-common

# Install python3
RUN \
  apt-get update && \
  apt-get install -y python3 python3-dev python3-pip python3-virtualenv && \
  pip3 install goto-statement

# Install git \
RUN \
  apt-get install -y git && \
  git --version

# Install text editor \
RUN \
  apt-get update && \
  apt-get install -y emacs && \
  rm -rf /var/lib/apt/lists/*

# Remove unneccessary files
RUN \
  apt-get clean && \
  rm -rf /var/lib/apt/lists/*

# Set up conda environments for various python versions
RUN mkdir -p /home/external_validity
COPY ./setup.sh /home/setup.sh

# Make the setup.sh script executable and run it
RUN chmod +x /home/setup.sh && \
    /bin/bash -c "source /home/setup.sh"

# Set working directory
WORKDIR /home/external_validity

ENTRYPOINT /bin/bash