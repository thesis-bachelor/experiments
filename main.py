import argparse

from analysis import Analysis
from artifacts import OsirisArtifact, SnifferDogArtifact
from preprocessing import RelancerRepositoriesAnalyzer, SnifferDogRepositoriesAnalyzer


def main(args):
    osiris_artifact = OsirisArtifact()
    snifferdog_artifact = SnifferDogArtifact()
    analysis = Analysis()

    if args.command == 'preprocess':
        print('Cleaning up subjects from Relancer & SnifferDog datasets...')
        relancer_analyzer = RelancerRepositoriesAnalyzer()
        snifferdog_analyzer = SnifferDogRepositoriesAnalyzer()

        relancer_analyzer.start()
        snifferdog_analyzer.start()

    elif args.command == 'baseline':
        print('Important! This command requires the preprocessed datasets to be available.')
        if args.artifact == 'osiris':
            print("Establishing baseline results for Osiris artifact...")
            osiris_artifact.establish_baseline()
        elif args.artifact == 'snifferdog':
            print("Establishing baseline results for SnifferDog artifact...")
            snifferdog_artifact.establish_baseline()

    elif args.command == 'validate':
        if args.artifact == 'osiris':
            print("Running Osiris on SnifferDog and Relancer datasets...")
            osiris_artifact.run_validation()
        elif args.artifact == 'snifferdog':
            print("Running SnifferDog on the Relancer dataset...")
            snifferdog_artifact.run_validation()

    elif args.command == 'report':
        if args.artifact == 'raw_data':
            print("Generating venn diagram showing overlap between raw datasets...")
            analysis.show_venn_diagram_provided_raw_datasets()
        elif args.artifact == 'osiris':
            print("Generating report for Osiris artifact...")
            analysis.show_results_osiris()
        elif args.artifact == 'snifferdog':
            print("Generating report for SnifferDog artifact...")
            analysis.show_results_snifferdog()
        elif args.artifact == 'all':
            print("Generating all results...")
            analysis.show_all_results()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Run the experiments from the paper.')
    parser.add_argument('command', type=str, help='Enter your command: preprocess, baseline, validate',
                        choices=['preprocess', 'baseline', 'validate', 'report'])
    parser.add_argument('--artifact', type=str, help='Enter the artifact: osiris, snifferdog',
                        choices=['osiris', 'snifferdog', 'all'])

    argv = parser.parse_args()

    if 'command' not in argv:
        parser.print_help()
    else:
        main(argv)
