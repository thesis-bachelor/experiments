import os
import subprocess
import json
from pathlib import Path

import requests
from git import Repo

from constants import PROJECT_DIR, RELANCER_NOTEBOOKS_DIR, RELANCER_DATASET, RELANCER_SAMPLE, \
    NOTEBOOK_CONVERSION_LOG, SNIFFERDOG_SAMPLE, SNIFFERDOG_DATASET, ORIGINAL_REPOSITORIES_DIR, RELANCER_RAW_DATASET, \
    SNIFFERDOG_RAW_DATASET
from logs.helper import ExperimentLogger
from my_secrets import GITHUB_TOKEN
from threader import do_threading

logger = ExperimentLogger.init_logger("preprocessing")


class Analyzer:
    @staticmethod
    def py_script_to_ipynb(py_script_path):
        ipynb_path = py_script_path.replace(".py", ".ipynb")
        command = f'ipynb-py-convert "{py_script_path}" "{ipynb_path}"'

        try:
            process = subprocess.run(command, shell=True, check=True)
            return process.returncode == 0
        except subprocess.CalledProcessError as e:
            logger.error(f"Error converting {py_script_path} to {ipynb_path}: {e}")
            return False

    @staticmethod
    def ipynb_to_py_script(ipynb_path):
        command = f'jupyter nbconvert --to script "{ipynb_path}" 1 >> {NOTEBOOK_CONVERSION_LOG} 2>&1'

        try:
            process = subprocess.run(command, shell=True, check=True)
            return process.returncode == 0
        except subprocess.CalledProcessError as e:
            logger.error(f"Error converting {ipynb_path} to py script: {e}")
            return False

    def get_notebook_paths(self, original_repos_dir, project_name, to_script=False, to_ipynb=False):
        if not to_script and not to_ipynb:
            print("Please set either to_script or to_ipynb to True")
            return None

        notebook_paths = []
        notebook_paths_ipynb = []
        repo_dir = original_repos_dir.replace(f"{PROJECT_DIR}/", "")
        project_path = os.path.join(repo_dir, project_name)

        for root, dirs, files in os.walk(project_path):
            for file in files:
                file_path = os.path.join(repo_dir, project_name, file)
                file_path_converted = None
                conversion_function = None

                if to_ipynb and str(file).endswith(".py"):
                    notebook_paths.append(file_path)
                    file_path_converted = file_path.replace(".py", ".ipynb")
                    conversion_function = self.py_script_to_ipynb
                elif to_script and str(file).endswith(".ipynb"):
                    notebook_paths_ipynb.append(file_path)
                    file_path_converted = file_path.replace(".ipynb", ".py")
                    conversion_function = self.ipynb_to_py_script

                if file_path_converted is not None and not os.path.exists(file_path_converted):
                    is_converted = conversion_function(file_path)
                    if is_converted:
                        if to_ipynb:
                            notebook_paths_ipynb.append(file_path_converted)
                        elif to_script:
                            notebook_paths.append(file_path_converted)
                    else:
                        if to_script:
                            notebook_paths.append(file_path)

        return {
            "notebook_paths": notebook_paths,
            "notebook_count": len(notebook_paths),
            "notebook_paths_ipynb": notebook_paths_ipynb,
            "notebook_count_ipynb": len(notebook_paths_ipynb)
        }


class RelancerRepositoriesAnalyzer(Analyzer):
    def start(self):
        do_threading(RELANCER_RAW_DATASET, RELANCER_DATASET, self.analyze_kaggle_notebooks, 8)

    @staticmethod
    def get_dataset_metadata(dataset_name):
        metadata_path = f"{PROJECT_DIR}/data/kaggle/{dataset_name}"
        command = f"kaggle datasets metadata -p {metadata_path} {dataset_name}"

        metadata_file = f"{metadata_path}/dataset-metadata.json"
        try:
            subprocess.run(command, shell=True, check=True)
            with open(metadata_file, 'r') as f:
                metadata = json.load(f)
                filtered_metadata = {
                    "usability_rating": metadata["usabilityRating"],
                    "has_usability_rating": metadata["hasUsabilityRating"],
                    "total_views": metadata["totalViews"],
                    "total_votes": metadata["totalVotes"],
                    "total_downloads": metadata["totalDownloads"],
                }
                return filtered_metadata, None
        except Exception as e:
            logger.error(f"Error while reading {metadata_file}: {e}")
            return None, str(e)

    def analyze_kaggle_notebooks(self, df):
        results = []
        for _, row in df.iterrows():
            project_name = row['project name']
            dataset_name = project_name.replace("_", "/")
            python_version = row['python version']
            logger.info(f"Analyzing {project_name}")

            base_info = {
                'id_name': project_name,
                'dataset_origin': ['relancer'],
                'python_version': python_version,
            }

            # Find the number of notebooks in the project and record their paths
            notebook_data = self.get_notebook_paths(RELANCER_NOTEBOOKS_DIR, project_name, to_ipynb=True)
            base_info.update(notebook_data)

            # Get dataset metadata
            metadata, err = self.get_dataset_metadata(dataset_name)

            if metadata:
                base_info.update(metadata)
            base_info['errors'] = err

            logger.info(f"{project_name} results: {base_info}")

            # add new row to results
            results.append(base_info)

        return results


class SnifferDogRepositoriesAnalyzer(Analyzer):
    def start(self):
        do_threading(SNIFFERDOG_RAW_DATASET, SNIFFERDOG_DATASET, self.analyze_snifferdog_notebooks)

    @staticmethod
    def clone_repository(git_url, id_name):
        """Clone the repository from git to the original_repositories folder."""
        cloning_exception = None
        github_api_exception = None

        # Create an ID from the Git URL: This will form the path where the repo will be cloned into.
        # Example: https://www.github.com/kb000/ragnar_race will have the ID "kb000@ragnar_race"

        base_url, name, project = git_url.replace('https://', '').split("/")

        remote = f"https://{GITHUB_TOKEN}@{base_url.lstrip('www.')}/{name}/{project}.git"
        repo_path = Path(ORIGINAL_REPOSITORIES_DIR, id_name)

        try:
            Repo.clone_from(remote, repo_path)
        except Exception as e:
            cloning_exception = e

        # Get the number of stars, watchers and forks from the GitHub API
        headers = {"Authorization": f"token {GITHUB_TOKEN}"}
        github_api = f"https://api.github.com/repos/{name}/{project}"
        response = requests.get(github_api, headers=headers)
        if response.status_code == 200:
            data = response.json()
            stars = data["stargazers_count"]
            watchers = data["subscribers_count"]
            forks = data["forks_count"]
        else:
            stars = watchers = forks = None
            github_api_exception = response.text

        return repo_path, {
            "git_url": git_url,
            "stars": stars,
            "watchers": watchers,
            "forks": forks,
            "cloning_exception": cloning_exception,
            "github_api_exception": github_api_exception,
        }

    @staticmethod
    def get_environment_info(repo_path):
        files = os.listdir(repo_path)
        environment_files = [file for file in files if file.endswith(".yml") or file.endswith(".yaml")]
        if environment_files:
            return environment_files[0]
        elif "requirements.txt" in files:
            return "requirements.txt"
        elif "Pipefile" in files:
            return "Pipefile"
        else:
            return None

    def analyze_snifferdog_notebooks(self, df):
        results = []
        for _, row in df.iterrows():
            git_url = row['GitHub URL']
            id_name = row['ID name']
            py_version = row['Python Version']

            logger.info(f"Analyzing {id_name}")
            base_info = {
                'id_name': id_name,
                'git_url': git_url,
                'dataset_origin': ['snifferdog'],
                'python_version': py_version,
            }

            # Check if the repository is cloned
            if os.path.exists(f"{ORIGINAL_REPOSITORIES_DIR}/{id_name}"):
                results.append(base_info)
                continue
            repo_path, repo_data = self.clone_repository(git_url, id_name)
            base_info.update(repo_data)

            if repo_data["cloning_exception"]:
                logger.error(f"Error while cloning {git_url}: {repo_data['cloning_exception']}")
                results.append(base_info)
                continue

            # get environment info
            environment_file = self.get_environment_info(repo_path)
            if environment_file:
                base_info['environment_dependency_info'] = environment_file

            # Find the number of notebooks in the project and record their paths
            notebook_data = self.get_notebook_paths(ORIGINAL_REPOSITORIES_DIR, id_name, to_script=True)
            base_info.update(notebook_data)

            logger.info(f"{id_name} results: {base_info}")

            # add new row to results
            results.append(base_info)

        return results

