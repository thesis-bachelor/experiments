from my_secrets import *

DEPRECATED_API_ERRORS = [
    ModuleNotFoundError, ImportError, TypeError, ValueError, AttributeError
]

CONDA_SH = "/opt/conda/etc/profile.d/conda.sh"
ENV_PREFIX = 'env_'

PROJECT_DIR = "/home/external_validity"

# ARTIFACTS
RELANCER_NOTEBOOKS_DIR = f"{PROJECT_DIR}/artifacts/relancer/relancer-exp/original_notebooks"

OSIRIS_DIR = f"{PROJECT_DIR}/artifacts/osiris"
OSIRIS_BINARY = f"{PROJECT_DIR}/artifacts/osiris/runOsiris.sh"

SNIFFERDOG_DIR = f"{PROJECT_DIR}/artifacts/snifferdog"
SNIFFERDOG_YML_DIR = f"{SNIFFERDOG_DIR}/yml"

# DATA DIRECTORIES
ORIGINAL_REPOSITORIES_DIR = f"{PROJECT_DIR}/original_repositories"

# INPUT
INPUT_DIR = f"{PROJECT_DIR}/data/input"
RAW_DATASETS_DIR = f"{INPUT_DIR}/raw_repositories_dataset"
RAW_NOTEBOOKS_DIR = f"{INPUT_DIR}/raw_notebooks_dataset"
SAMPLE_INPUT_DIR = f"{INPUT_DIR}/sample_repositories_dataset"

# RAW DATASETS
SNIFFERDOG_RAW_DATASET = f"{RAW_DATASETS_DIR}/dataset_snifferdog.csv"
RELANCER_RAW_DATASET = f"{RAW_DATASETS_DIR}/dataset_relancer.csv"
OSIRIS_RAW_DATASET = f"{RAW_DATASETS_DIR}/dataset_osiris.csv"

# RAW NOTEBOOKS
SNIFFERDOG_RAW_NOTEBOOKS = f"{RAW_NOTEBOOKS_DIR}/dataset_snifferdog.csv"
RELANCER_RAW_NOTEBOOKS = f"{RAW_NOTEBOOKS_DIR}/dataset_relancer.csv"
OSIRIS_RAW_NOTEBOOKS = f"{RAW_NOTEBOOKS_DIR}/dataset_osiris.csv"

# SAMPLE INPUT DATASETS
SNIFFERDOG_SAMPLE = f"{SAMPLE_INPUT_DIR}/dataset_sample_snifferdog.csv"
RELANCER_SAMPLE = f"{SAMPLE_INPUT_DIR}/dataset_sample_relancer.csv"
OSIRIS_SAMPLE = f"{SAMPLE_INPUT_DIR}/dataset_sample_osiris.csv"

# LOGS
NOTEBOOK_CONVERSION_LOG = f"{PROJECT_DIR}/logs/notebook_conversion.log"

# PROCESSED DATASETS
PROCESSED_DATASETS_DIR = f"{INPUT_DIR}/processed_repositories"
RELANCER_DATASET = f"{PROCESSED_DATASETS_DIR}/dataset_relancer.csv"
SNIFFERDOG_DATASET = f"{PROCESSED_DATASETS_DIR}/dataset_snifferdog.csv"
OSIRIS_DATASET = f"{PROCESSED_DATASETS_DIR}/dataset_osiris.csv"

# OUTPUT
OUTPUT_DIR = f"{PROJECT_DIR}/data/output"
SNIFFERDOG_BASELINE_RESULTS = f"{OUTPUT_DIR}/snifferdog_baseline.csv"
SNIFFERDOG_VALIDATION_RESULTS_ON_RELANCER = f"{OUTPUT_DIR}/snifferdog_validation_on_relancer.csv"

OSIRIS_BASELINE_RESULTS = f"{OUTPUT_DIR}/osiris_baseline_results.csv"
OSIRIS_VALIDATION_RESULTS_ON_SNIFFERDOG = f"{OUTPUT_DIR}/osiris_validation_results_on_snifferdog.csv"
OSIRIS_VALIDATION_RESULTS_ON_RELANCER = f"{OUTPUT_DIR}/osiris_validation_results_on_relancer.csv"

