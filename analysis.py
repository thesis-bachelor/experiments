import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from matplotlib_venn import venn3

from constants import RELANCER_RAW_NOTEBOOKS, OSIRIS_RAW_NOTEBOOKS, SNIFFERDOG_RAW_NOTEBOOKS, OSIRIS_BASELINE_RESULTS, \
    SNIFFERDOG_BASELINE_RESULTS, OSIRIS_VALIDATION_RESULTS_ON_SNIFFERDOG, OSIRIS_VALIDATION_RESULTS_ON_RELANCER, \
    SNIFFERDOG_VALIDATION_RESULTS_ON_RELANCER


def calculate_total_and_percentage(x, total_count):
    total = x.sum()
    if total != 0:
        percentage = np.round((total / total_count) * 100, 2)
        return f"{int(total)}({percentage}%)"
    else:
        return "0(0%)"


def calculate_counts_for_snifferdog(data):
    total_repos = len(data['id_name'].unique())
    data = data.drop_duplicates()
    data = data.dropna(subset=['notebook_path'])
    total_notebooks = len(data)

    empty_repos = total_repos - len(data['id_name'].unique())
    notebooks_with_dependencies = len(data[data['Dependencies'].notnull()])
    repos_with_dependencies = len(data[data['Dependencies'].notnull()]['id_name'].unique())

    missing_packages_in_api_bank = len(
        data[data['Generated Requirements'].str.contains(
            'Might contain some packages that are not included in our API Bank database! It would be great if you can report this to us!',
            na=False)])

    execution_environments = len(data[data['Environment Created'] == True])
    repos_with_envs_created = len(data[data['Environment Created'] == True]['id_name'].unique())

    executable_notebooks = len(data[(data['Environment Created'] == True) & data['error'].isnull()])
    repos_with_executable_notebooks = len(
        data[(data['Environment Created'] == True) & data['error'].isnull()]['id_name'].unique())

    return total_repos, total_notebooks, empty_repos, notebooks_with_dependencies, repos_with_dependencies, \
        missing_packages_in_api_bank, execution_environments, repos_with_envs_created, executable_notebooks, \
        repos_with_executable_notebooks


class Analysis:
    """ Class for analysis of results."""

    def __init__(self):
        pass

    @staticmethod
    def show_venn_diagram_provided_raw_datasets():
        """ Show venn diagram of provided raw datasets."""
        print('Figure 5.1: Venn diagram depicting the overlap of the datasets from the research artifacts.')

        # Load data
        relancer = pd.read_csv(RELANCER_RAW_NOTEBOOKS)
        osiris = pd.read_csv(OSIRIS_RAW_NOTEBOOKS)
        snifferdog = pd.read_csv(SNIFFERDOG_RAW_NOTEBOOKS)

        # Clean up
        relancer = relancer[relancer['repository_name'] != 'None']
        snifferdog = snifferdog[snifferdog['repository_name'] != 'None']
        osiris = osiris[osiris['repository_name'] != 'None']

        # Create sets
        relancer_set = set(relancer['repository_name'])
        snifferdog_set = set(snifferdog['repository_name'])
        osiris_set = set(osiris['repository_name'])

        # Plot the venn diagram
        venndiagram = venn3([relancer_set, snifferdog_set, osiris_set], set_labels=('Relancer', 'SnifferDog', 'Osiris'),
                            set_colors=('red', 'green', 'blue'), alpha=0.3)

        # Create legend
        handles = [venndiagram.get_patch_by_id('100'),
                   venndiagram.get_patch_by_id('010'),
                   venndiagram.get_patch_by_id('001'),
                   venndiagram.get_patch_by_id('011')]

        labels = [venndiagram.get_label_by_id('100').get_text(),
                  venndiagram.get_label_by_id('010').get_text(),
                  venndiagram.get_label_by_id('001').get_text(),
                  venndiagram.get_label_by_id('011').get_text()]

        venndiagram.get_label_by_id('100').set_text('')
        venndiagram.get_label_by_id('010').set_text('')
        venndiagram.get_label_by_id('001').set_text('')
        venndiagram.get_label_by_id('011').set_text('')

        legend = plt.legend(
            handles, labels, title='Repository Count', loc='upper left', bbox_to_anchor=(1.02, 1), fontsize=8)
        plt.setp(legend.get_title(), fontsize=11)
        plt.title('Venn diagram depicting the overlap of the datasets from the research artifacts.')
        plt.show()

    @staticmethod
    def generate_osiris_topdown_strategy_report(data, label):
        """ Generate report for topdown strategy of Osiris artifact."""
        data['python_version'] = data['python_version'].replace(np.nan, 'None')

        # Calculate totals and percentages for each Python version
        summary = data.groupby(['python_version']).agg(
            Total=('id_name', 'count'),
            Executable_Notebooks=('executability', lambda x: x.sum()),
        )

        # Convert 'Total' column to integers
        summary['Total'] = summary['Total'].astype(int)

        # Calculate the percentage only if 'Total' is not zero
        summary['Percentage'] = np.where(summary['Total'] != 0,
                                         summary['Executable_Notebooks'] / summary['Total'] * 100,
                                         0)

        # Format the 'Percentage' column as a string with two decimal places
        summary['Percentage'] = summary['Percentage'].apply(lambda x: f"{x:.2f}%")

        # Calculate the totals row
        total_row = pd.DataFrame({
            'Total': [summary['Total'].sum()],
            'Executable_Notebooks': [summary['Executable_Notebooks'].sum()],
            'Percentage': [summary['Executable_Notebooks'].sum() / summary['Total'].sum() * 100]
        }, index=['Totals'])

        # Format the percentage value in the totals row
        total_row['Percentage'] = total_row['Percentage'].apply(lambda x: f"{x:.2f}%")

        # Append the totals row to the summary
        final_summary = pd.concat([summary, total_row])
        print(f"\n {label} \n{final_summary} \n")

    @staticmethod
    def generate_osiris_matching_strategies_report(data, label):
        """ Generate report for matching strategies of Osiris artifact."""
        data['python_version'] = data['python_version'].replace(np.nan, 'None')

        # calculate notebook count per python version
        notebook_counts = data.groupby(['python_version'])['id_name'].count()

        # compute different configurations
        summary = data.groupby('python_version').agg({
            'strong_matched_ratio': lambda x: calculate_total_and_percentage(x, notebook_counts.loc[x.name]),
            'weak_matched_ratio': lambda x: calculate_total_and_percentage(x, notebook_counts.loc[x.name]),
            'best_effort_matched_ratio': lambda x: calculate_total_and_percentage(x, notebook_counts.loc[x.name]),
        })

        # Combine the series and dataframe into a single dataframe
        summary = pd.concat([notebook_counts, summary], axis=1)

        # rename columns
        summary.columns = ['Notebooks', 'Strong', 'Weak', 'Best Effort']

        # calculate totals for each column
        total_notebooks = summary['Notebooks'].sum()
        total_strong = f"{int(data['strong_matched_ratio'].sum())}({np.round((data['strong_matched_ratio'].sum() / total_notebooks) * 100, 2)}%)"
        total_weak = f"{int(data['weak_matched_ratio'].sum())}({np.round((data['weak_matched_ratio'].sum() / total_notebooks) * 100, 2)}%)"
        total_best_effort = f"{int(data['best_effort_matched_ratio'].sum())}({np.round((data['best_effort_matched_ratio'].sum() / total_notebooks) * 100, 2)}%)"

        # Calculate the total row
        total_row = pd.DataFrame({
            'Notebooks': [total_notebooks],
            'Strong': [total_strong],
            'Weak': [total_weak],
            'Best Effort': [total_best_effort]
        }, index=['Total'])

        final_summary = pd.concat([summary, total_row])
        print(f"\n {label} \n{final_summary} \n")

    @staticmethod
    def generate_snifferdog_report(data, label):
        """ Generate report for SnifferDog artifact."""
        total_repos, total_notebooks, empty_repos, notebooks_with_dependencies, repos_with_dependencies, \
            missing_packages_in_api_bank, execution_environments, repos_with_envs_created, executable_notebooks, \
            repos_with_executable_notebooks = calculate_counts_for_snifferdog(data)

        # create dataframe
        summary_table = pd.DataFrame({
            'Metric': ['Total', 'Empty repositories', 'Generated dependencies', 'Missing packages in API Bank',
                       'Execution environments', 'Executable notebooks'],
            'Repositories': [total_repos, empty_repos, repos_with_dependencies, np.nan,
                             repos_with_envs_created, repos_with_executable_notebooks],
            'Notebooks': [total_notebooks, np.nan, notebooks_with_dependencies, missing_packages_in_api_bank,
                          execution_environments, executable_notebooks]
        })

        # Compute the percentage
        summary_table['Repositories'] = summary_table.apply(
            lambda
                row: f"{int(row['Repositories'])}({round((row['Repositories'] / (repos_with_dependencies if row['Metric'] in ['Execution environments', 'Executable notebooks', 'Missing packages in API Bank'] else total_repos)) * 100, 2)}%)"
            if pd.notna(row['Repositories']) else '-', axis=1)

        summary_table['Notebooks'] = summary_table.apply(
            lambda
                row: f"{int(row['Notebooks'])}({round((row['Notebooks'] / (notebooks_with_dependencies if row['Metric'] in ['Execution environments', 'Executable notebooks', 'Missing packages in API Bank'] else total_notebooks)) * 100, 2)}%)"
            if pd.notna(row['Notebooks']) else '-', axis=1)

        print(f"\n {label} \n{summary_table} \n")

    def show_baseline_results_osiris(self):
        """ Show baseline results for Osiris artifact."""
        # Load data
        data = pd.read_csv(OSIRIS_BASELINE_RESULTS)
        label_top_down = 'Figure 5.2: Top-down execution strategy configuration.'
        label_matching_strategy = \
            'Table 5.1: Execution rates of Osiris when varying the matching strategy configuration.'

        self.generate_osiris_topdown_strategy_report(data, label_top_down)
        self.generate_osiris_matching_strategies_report(data, label_matching_strategy)

    def show_validation_results_osiris(self):
        """ Show validation results for Osiris artifact."""
        data_validation_on_snifferdog = pd.read_csv(OSIRIS_VALIDATION_RESULTS_ON_SNIFFERDOG)
        label_top_down_on_snifferdog = \
            'Figure 5.3: Top-down execution strategy configuration.'
        label_matching_strategy_on_snifferdog = \
            'Table 5.3: Validation Results of Osiris Execution on Subjects Derived from the SnifferDog Dataset'

        #self.generate_osiris_topdown_strategy_report(data_validation_on_snifferdog, label_top_down_on_snifferdog)
        self.generate_osiris_matching_strategies_report(data_validation_on_snifferdog,
                                                        label_matching_strategy_on_snifferdog)

        data_validation_on_relancer = pd.read_csv(OSIRIS_VALIDATION_RESULTS_ON_RELANCER)
        label_top_down_on_relancer = \
            'Top-down execution strategy configuration on Relancer.'
        label_matching_strategy_on_relancer = \
            'Table 5.4: Validation Results of Osiris Execution on Subjects Derived from the Relancer Dataset'
        self.generate_osiris_topdown_strategy_report(data_validation_on_relancer, label_top_down_on_relancer)
        self.generate_osiris_matching_strategies_report(data_validation_on_relancer,
                                                        label_matching_strategy_on_relancer)

    def show_baseline_results_snifferdog(self):
        """ Show baseline results for SnifferDog artifact."""
        # Load data
        data = pd.read_csv(SNIFFERDOG_BASELINE_RESULTS)
        label = 'Table 5.2: Reproduction results of SnifferDog paper.'
        self.generate_snifferdog_report(data, label)

    def show_validation_results_snifferdog(self):
        """ Show validation results for SnifferDog artifact."""
        data = pd.read_csv(SNIFFERDOG_VALIDATION_RESULTS_ON_RELANCER)
        label = 'Table 5.5: Validation Results of SnifferDog Execution on Subjects Derived from the Relancer Dataset'
        self.generate_snifferdog_report(data, label)

    def show_results_osiris(self):
        """ Show results for Osiris artifact."""
        self.show_baseline_results_osiris()
        self.show_validation_results_osiris()

    def show_results_snifferdog(self):
        """ Show results for SnifferDog artifact."""
        self.show_baseline_results_snifferdog()
        self.show_validation_results_snifferdog()

    def show_all_results(self):
        """ Show all results."""
        self.show_venn_diagram_provided_raw_datasets()
        self.show_results_osiris()
        self.show_results_snifferdog()

