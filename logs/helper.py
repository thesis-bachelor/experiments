import logging

from constants import PROJECT_DIR


# Class for logging experiment results
class ExperimentLogger:
    @staticmethod
    def init_logger(name):
        logger = logging.getLogger(name)
        logger.setLevel(logging.INFO)

        filename = f"{PROJECT_DIR}/logs/{name}.log"

        # Create a file handler for the logger
        file_handler = logging.FileHandler(filename, mode='w')
        file_handler.setLevel(logging.INFO)

        logger.addHandler(file_handler)

        return logger
